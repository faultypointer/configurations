{config, ...}:
{
    programs.nixvim.plugins = {
	bufferline = {
	    enable = true;
	    alwaysShowBufferline = false;
	    bufferCloseIcon = null;
	    showCloseIcon = false;
	    closeIcon = null;
	    themable = true;
	    separatorStyle = "slant";
	    showBufferCloseIcons = false;
	 #    highlights = {
		# background = {
		#     bg = "#${config.stylix.base16Scheme.base00}";
		# };
		# fill = {
		#     bg = "#${config.stylix.base16Scheme.base00}";
		# };
		# separator = {
		#     bg = "#${config.stylix.base16Scheme.base00}";
		#     fg = "#${config.stylix.base16Scheme.base00}";
		# };
		# separatorSelected = {
		#     fg = "#${config.stylix.base16Scheme.base00}";
		# };
		# separatorVisible = {
		#     fg = "#${config.stylix.base16Scheme.base00}";
		# };
	 #    };
	};
    };
}
