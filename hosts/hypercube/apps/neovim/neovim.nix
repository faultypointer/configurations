{
    config, lib, ...
}:
{
    programs.nixvim = {
	enable = true;
	opts = {
	    incsearch = true; # Incremental search: show match for partly typed search command
	    inccommand = "split"; # Search and replace: preview changes in quickfix list
	    ignorecase = true; # When the search query is lower-case, match both lower and upper-case
	    smartcase = true; # Override the 'ignorecase' option if the search pattern contains upper
	    number = true;
	    relativenumber = true;
	    shiftwidth = 4;
	    tabstop = 4;
	    numberwidth = 4;
	    undofile = true;
	    autoindent =true; # Do clever autoindenting
	    wrap = true;
	    linebreak = true; # Wrap lines after words
	    updatetime = 100; # Faster Completion
		signcolumn = "yes";
	    fillchars = {
		eob = " ";
	    };
	};
        highlight = {
	    WinSeparator.fg = "#${config.stylix.base16Scheme.base00}";
	};

	clipboard.register = "unnamedplus";
	globals.mapleader = " ";

	# colorschemes.gruvbox = {
	#     enable = true;
	#     settings = {
	# 	undercurl = true;
	# 	underline = true;
	# 	bold = true;
	# 	italic = {
	# 	    strings = true;
	# 	    emphasis = true;
	# 	    comments = true;
	# 	    operators = false;
	# 	    folds = true;
	# 	};
	# 	strikethrough = true;
	# 	invert_selection = false;
	# 	invert_signs = false;
	# 	invert_tabline = false;
	# 	invert_intend_guides = false;
	# 	inverse = true; #- invert background for search, diffs, statuslines and errors
	# 	palette_overrides = {};
	# 	overrides = {};
	# 	dim_inactive = false;
	# 	transparent_mode = true;
	#     };
	# };
	#
	colorschemes.base16 = {
	    enable = true;
	    customColorScheme = {
		base00 = "#${config.stylix.base16Scheme.base00}";
		base01 = "#${config.stylix.base16Scheme.base01}";
		base02 = "#${config.stylix.base16Scheme.base02}";
		base03 = "#${config.stylix.base16Scheme.base03}";
		base04 = "#${config.stylix.base16Scheme.base04}";
		base05 = "#${config.stylix.base16Scheme.base05}";
		base06 = "#${config.stylix.base16Scheme.base06}";
		base07 = "#${config.stylix.base16Scheme.base07}";
		base08 = "#${config.stylix.base16Scheme.base08}";
		base09 = "#${config.stylix.base16Scheme.base09}";
		base0A = "#${config.stylix.base16Scheme.base0A}";
		base0B = "#${config.stylix.base16Scheme.base0B}";
		base0C = "#${config.stylix.base16Scheme.base0C}";
		base0D = "#${config.stylix.base16Scheme.base0D}";
		base0E = "#${config.stylix.base16Scheme.base0E}";
		base0F = "#${config.stylix.base16Scheme.base0F}";
	    };
	};
    };
}
